package com.rajim.integration.controller;

import com.rajim.integration.entity.Person;
import com.rajim.integration.service.PersonService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@Slf4j
public class PersonController {

	@Autowired
	private PersonService service;

	@GetMapping("/getAllPersons")
	public List<Person> getPersons() {
		log.info("Controller getPersons() method called...");
		return service.findAllPersons();
	}

	@PostMapping("/savePerson")
	public Person savePerson(@RequestBody Person person) {
		log.info("Controller savePerson() method called...");
		return service.savePerson(person);
	}
}
