package com.rajim.integration.service;

import java.util.List;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.rajim.integration.entity.Person;
import com.rajim.integration.repository.PersonRepository;

@Service
@Slf4j
public class PersonService {

	@Autowired
	private PersonRepository repository;

	public Person savePerson(Person person) {
		log.info("Service savePerson() method Called...");
		return repository.save(person);
	}
	
	public List<Person> findAllPersons() {
		log.info("Service findAllPersons() method Called...");
		return repository.findAll();
	}

}
